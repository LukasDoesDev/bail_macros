mod continue_tests {
    use crate::*;

    #[test]
    fn continue_test() {
        for _ in [0] {
            cob!(None);
            panic!("cob didn't continue");
        }
        for _ in [0] {
            crb!(Err(0));
            panic!("crb didn't continue");
        }
        for _ in [0] {
            cfb!(false);
            panic!("cfb didn't continue");
        }
        for _ in [0] {
            ctb!(true);
            panic!("ctb didn't continue");
        }
    }
}

mod return_tests {
    use crate::*;

    #[test]
    fn return_test() {
        (|| {
            rob!(None);
            panic!("rob didn't return");
        })();
        (|| {
            rrb!(Err(0));
            panic!("rrb didn't return");
        })();
        (|| {
            rfb!(false);
            panic!("rfb didn't return");
        })();
        (|| {
            rtb!(true);
            panic!("rtb didn't return");
        })();
    }

    #[test]
    fn return_false_test() {
        let x = (|| {
            rfob!(None);
            panic!("rfob didn't return");
        })();
        assert!(!x, "rfob didn't return false");
        let x = (|| {
            rfrb!(Err(0));
            panic!("rfrb didn't return");
        })();
        assert!(!x, "rfrb didn't return false");
        let x = (|| {
            rffb!(false);
            panic!("rffb didn't return");
        })();
        assert!(!x, "rffb didn't return false");
        let x = (|| {
            rftb!(true);
            panic!("rftb didn't return");
        })();
        assert!(!x, "rftb didn't return false");
    }

    #[test]
    fn return_true_test() {
        let x = (|| {
            rtob!(None);
            panic!("rtob didn't return");
        })();
        assert!(x, "rfob didn't return true");
        let x = (|| {
            rtrb!(Err(0));
            panic!("rtrb didn't return");
        })();
        assert!(x, "rtrb didn't return true");
        let x = (|| {
            rtfb!(false);
            panic!("rtfb didn't return");
        })();
        assert!(x, "rtfb didn't return true");
        let x = (|| {
            rttb!(true);
            panic!("rttb didn't return");
        })();
        assert!(x, "rttb didn't return true");
    }

    #[test]
    fn return_none_test() {
        let x: Option<usize> = (|| {
            rnob!(None);
            panic!("rnob didn't return");
        })();
        assert_eq!(x, None, "rnob didn't return None");
        let x: Option<usize> = (|| {
            rnrb!(Err(0));
            panic!("rnrb didn't return");
        })();
        assert_eq!(x, None, "rnrb didn't return None");
        let x: Option<usize> = (|| {
            rnfb!(false);
            panic!("rnfb didn't return");
        })();
        assert_eq!(x, None, "rnfb didn't return None");
        let x: Option<usize> = (|| {
            rntb!(true);
            panic!("rntb didn't return");
        })();
        assert_eq!(x, None, "rntb didn't return None");
    }
}

mod break_tests {
    use crate::*;

    #[test]
    fn break_test() {
        for _ in [0] {
            bob!(None);
            panic!("bob didn't break");
        }
        for _ in [0] {
            brb!(Err(0));
            panic!("brb didn't break");
        }
        for _ in [0] {
            bfb!(false);
            panic!("bfb didn't break");
        }
        for _ in [0] {
            btb!(true);
            panic!("btb didn't break");
        }
    }

    #[test]
    fn break_false_test() {
        let x = loop {
            bfob!(None);
            panic!("bfob didn't break");
        };
        assert!(!x, "bfob didn't break false");
        let x = loop {
            bfrb!(Err(0));
            panic!("bfrb didn't break");
        };
        assert!(!x, "bfrb didn't break false");
        let x = loop {
            bffb!(false);
            panic!("bffb didn't break");
        };
        assert!(!x, "bffb didn't break false");
        let x = loop {
            bftb!(true);
            panic!("bftb didn't break");
        };
        assert!(!x, "bftb didn't break false");
    }

    #[test]
    fn break_true_test() {
        let x = loop {
            btob!(None);
            panic!("btob didn't break");
        };
        assert!(x, "btob didn't break true");
        let x = loop {
            btrb!(Err(0));
            panic!("btrb didn't break");
        };
        assert!(x, "btrb didn't break true");
        let x = loop {
            btfb!(false);
            panic!("btfb didn't break");
        };
        assert!(x, "btfb didn't break true");
        let x = loop {
            bttb!(true);
            panic!("bttb didn't break");
        };
        assert!(x, "bttb didn't break true");
    }

    #[test]
    fn break_none_test() {
        let x: Option<usize> = loop {
            bnob!(None);
            panic!("bnob didn't break");
        };
        assert_eq!(x, None, "bnob didn't return None");
        let x: Option<usize> = loop {
            bnrb!(Err(0));
            panic!("bnrb didn't break");
        };
        assert_eq!(x, None, "bnrb didn't return None");
        let x: Option<usize> = loop {
            bnfb!(false);
            panic!("bnfb didn't break");
        };
        assert_eq!(x, None, "bnfb didn't return None");
        let x: Option<usize> = loop {
            bntb!(true);
            panic!("bntb didn't break");
        };
        assert_eq!(x, None, "bntb didn't return None");
    }
}
