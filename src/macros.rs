macro_rules! generate_bail_macro {
    ($statement:expr, $name:ident, option) => {
        ::paste::paste! {
            #[doc = concat!("`", stringify!($statement), "` if input is [`Option::None`]")]
            #[macro_export]
            macro_rules! [<$name ob >] {
                ($res:expr) => {
                    let val: ::std::option::Option<_> = $res;
                    match val {
                        Some(v) => v,
                        None => {
                            $statement
                        }
                    }
                };
            }
        }
    };
    ($statement:expr, $name:ident, result) => {
        ::paste::paste! {
            #[doc = concat!("`", stringify!($statement), "` if input is [`Result::Err`]")]
            #[macro_export]
            macro_rules! [<$name rb >] {
                ($res:expr) => {
                    match $res {
                        Ok(v) => v,
                        Err(_e) => {
                            $statement
                        }
                    }
                };
            }
        }
    };
    ($statement:expr, $name:ident, false) => {
        ::paste::paste! {
            #[doc = concat!("`", stringify!($statement), "` if input is [`false`]")]
            #[macro_export]
            macro_rules! [<$name fb >] {
                ($res:expr) => {
                    let val: ::std::primitive::bool = $res;
                    if (!val) {
                        $statement
                    }
                };
            }
        }
    };
    ($statement:expr, $name:ident, true) => {
        ::paste::paste! {
            #[doc = concat!("`", stringify!($statement), "` if input is [`true`]")]
            #[macro_export]
            macro_rules! [<$name tb >] {
                ($res:expr) => {
                    let val: ::std::primitive::bool = $res;
                    if (val) {
                        $statement
                    }
                };
            }
        }
    };
    ($statement:expr, $name:ident) => {
        generate_bail_macro!($statement, $name, option);
        generate_bail_macro!($statement, $name, result);
        generate_bail_macro!($statement, $name, false);
        generate_bail_macro!($statement, $name, true);
    };
}

// bob! break, option, bail
// rffb! return false, false, bail
// bnrb! break none, result, bail

generate_bail_macro!(continue, c);

generate_bail_macro!(return, r);
generate_bail_macro!(return false, rf);
generate_bail_macro!(return true, rt);
generate_bail_macro!(return None, rn);

generate_bail_macro!(break, b);
generate_bail_macro!(break false, bf);
generate_bail_macro!(break true, bt);
generate_bail_macro!(break None, bn);
