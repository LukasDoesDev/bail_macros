# Bail Macros

[![Crates.io](https://img.shields.io/crates/v/bail_macros)](https://crates.io/crates/bail_macros)
[![Lib.rs](https://img.shields.io/badge/lib.rs-bail__macros-lightgrey)](https://lib.rs/crates/bail_macros)
[![Docs.rs](https://img.shields.io/badge/docs.rs-bail__macros-orange)](https://docs.rs/bail_macros)
![License](https://img.shields.io/crates/l/bail_macros)

Bail macros for Options, Results and more

## Quick Example
```rs
use bail_macros::bob;

for i in 0..=20 {
    println!("Index: {}", i);
    bob!(None);
    panic!("bob didn't break");
}
println!("cob broke out of the loop!");
```
