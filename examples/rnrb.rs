use bail_macros::rnrb;

fn external_api() -> Result<u8, String> {
    Err("An error occured".to_string())
}

fn get_number() -> Option<u8> {
    let value = rnrb!(external_api());
    println!("Unexpected value: {}", value);
    Some(42)
}

fn main() {
    let number_option = get_number();
    println!("number: {:?}", number_option);
}
