use bail_macros::bob;

fn main() {
    for i in 0..=20 {
        println!("Index: {}", i);
        bob!(None);
        panic!("bob didn't break");
    }
    println!("cob broke out of the loop!");
}
