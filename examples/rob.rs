use bail_macros::rob;

fn rob_test() {
    rob!(None);
    panic!("rob didn't return");
}

fn main() {
    rob_test();
    println!("rob returned!");
}
