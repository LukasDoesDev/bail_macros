use bail_macros::rfrb;

fn external_api() -> Result<u8, String> {
    Err("An error occured".to_string())
}

fn my_function() -> bool {
    let value = rfrb!(external_api());
    println!("Unexpected value: {}", value);
    true
}

fn main() {
    let successful = my_function();
    println!("successful: {}", successful);
}
